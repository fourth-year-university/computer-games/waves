Wave = {}
Wave.__index = Wave

function Wave:create(amplitude, angleVel, angle, width, height, moveX, moveY, c1, c2)
    local wave = {}
    setmetatable(wave, Wave)
    wave.amplitude = amplitude or 0
    wave.angleVel = angleVel or 0
    wave.angle = angle or 0
    wave.width = width or 0
    wave.height = height or 0
    wave.moveX = moveX or 0
    wave.moveY = moveY or 0
    wave.c1 = c1
    wave.c2 = c2
    return wave
end

function Wave:draw()
    for x = 0, self.width, 8 do
        y = self.amplitude * math.sin((self.angle + x / 24 / 10) * 4)
        y = y + self.amplitude * math.sin((self.angle + x / 24 / 10) * 7)
        love.graphics.setColor(self.c1[1], self.c1[2], self.c1[3])
        love.graphics.circle("line", x + self.moveX, y + self.height / 2, 10)
        love.graphics.setColor(self.c2[1], self.c2[2], self.c2[3], 0.5)
        love.graphics.circle("fill", x + self.moveX, y + self.height / 2, 10)
    end
    self.angle = self.angle + self.angleVel
end